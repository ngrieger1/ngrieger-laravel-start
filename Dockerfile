# Set master image
FROM php:7.2-fpm-alpine

RUN apk update
RUN apk add libxml2-dev
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install pdo

COPY ./.env.pipelines /var/www/.env